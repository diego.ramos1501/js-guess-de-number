const readline = require('readline'); // es un módulo para interactuar por consola, por ahora

var bienvenidaFunction = require('./librerias/bienvenida.js');

var lector = readline.createInterface({
	input: process.stdin,
	output: process.stdout
}); //Interfáz:frontera de comunicación
var jugador = {};
var nombreJugador;
var tiempoInicio;
var arrJugadores = new Array();

function bienvenidaCallback(nombre){
    //console.time('pregunta_nombre');
    nombreJugador = nombre;
    jugador = {
        nombre : nombreJugador,
        tiempo : 0
    }
    arrJugadores.push(jugador);
    console.clear();
    menu();
}

function menu(){
    
    console.log('MENÚ');
    console.log('1.- Jugar');
    console.log('2.- Mejores puntajes');
    console.log('3.- Cambiar jugador');
    console.log('4.- Salir');
    
    lector.question('Escriba su opción: ', opcion=>{
        switch (opcion) {
            case '1':
                console.clear();
                jugar();
                break;
            case '2':
                console.clear();
                console.log('Mejores jugadores');
                mejoresTiempos();
                menu();
                break;
            case '4':
                console.clear();
                console.log('Adios %s', nombreJugador);
                lector.close();
                process.exit(0);
                break;
            case '3':
                console.clear();
                bienvenida();
                break;
            default:
                break;
        }
    });
}

function jugar(){
    console.clear();
    console.log('Juguemos');
    
    let numeroIncognito = parseInt(Math.random() * 10);
    tiempoInicio = Date.now();
    consultar(numeroIncognito);
    
}

function consultar(numeroIncognito){
    lector.question('Ingrese un número: ', numero =>{
        numero = +numero; //transforma a entero
        if(numero < numeroIncognito){
            console.log('Le falta');
            consultar(numeroIncognito);
        }else if(numero > numeroIncognito){
            console.log('Le sobra');
            consultar(numeroIncognito);
        }else{
            console.log('Felicitaciones');
            verTiempo();
            menu();
        }
    });
}

function verTiempo(now){
    tiempoFin = Date.now();
    tiempoTotal = (tiempoFin - tiempoInicio)/1000;
    jugador.tiempo = tiempoTotal;
    console.log('Tu tiempo: %i segundos!!', tiempoTotal);
    

}

function mejoresTiempos(){
    arrJugadores.sort(function(a,b){
        return a.tiempo - b.tiempo;
    });
    arrJugadores.forEach(jugador => {
        console.log(jugador);
    });
}
bienvenidaFunction(lector, bienvenidaCallback);